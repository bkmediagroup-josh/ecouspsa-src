export const state = () => ({
  list: [
    {
      id: 1,
      shortName: "PSAC",
      location: "Aurora",
      schedules: {
        2019: {
          dayOfWeek: 1,
          weekOfMonth: 1,
          sectionQualifier: 9,
        },
        2020: {
          dayOfWeek: 1,
          weekOfMonth: 1,
          sectionQualifier: 9,
        },
        2021: {
          dayOfWeek: 1,
          weekOfMonth: 1,
          sectionQualifier: 8,
        },
        2022: {
          dayOfWeek: 1,
          weekOfMonth: 1,
          sectionQualifier: 8,
        },
      },
      name: 'Practical Shooters Association of Colorado',
      code: 'ECO08',
      hostClub: {
        url: 'http://www.auroragunclub.com',
        name: 'Aurora Gun Club',
      },
      times: {
        'Shooter&#39;s Meeting': '9:00 AM',
      },
      contacts: [
        {
          position: 'President',
          name: 'Josh Horner',
          email: 'josh@horner.im',
          phone: '720-980-1284'
        }
      ],
      fees: {
        'Fee': '$20',
      },
      googleMapsIframeUrl: 'https://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=301+South+Gun+Club+Road+Aurora,+CO+80018&aq=&sll=39.710587,-104.717817&sspn=0.024067,0.037165&ie=UTF8&hq=&hnear=301+S+Gun+Club+Rd,+Aurora,+Arapahoe,+Colorado+80018&t=m&ll=39.709564,-104.716187&spn=0.096402,0.124969&z=12&iwloc=&output=embed',
      googleMapLink: 'https://maps.google.com/maps?f=q&source=embed&hl=en&geocode=&q=301+South+Gun+Club+Road+Aurora,+CO+80018&aq=&sll=39.710587,-104.717817&sspn=0.024067,0.037165&ie=UTF8&hq=&hnear=301+S+Gun+Club+Rd,+Aurora,+Arapahoe,+Colorado+80018&t=m&ll=39.709564,-104.716187&spn=0.096402,0.124969&z=12&iwloc=',
    }, {
      id: 2,
      shortName: "CRCPS",
      location: "Byers",
      schedules: {
        2019: {
          dayOfWeek: 7,
          weekOfMonth: 2,
          sectionQualifier: 7,
          superClassifiers: [5],
          majors: {
            m9: '13-15<sup>2</sup>',
          },
        },
        2020: {
          dayOfWeek: 7,
          weekOfMonth: 2,
          sectionQualifier: 7,
          superClassifiers: [8],
        },
        2021: {
          dayOfWeek: 7,
          weekOfMonth: 2,
          sectionQualifier: 7,
        },
        2022: {
          dayOfWeek: 7,
          weekOfMonth: 2,
          sectionQualifier: 7,
        },
      },
      name: 'Colorado Rifle Club Practical Shooters',
      code: 'ECO15',
      hostClub: {
        url: 'http://www.crci.org',
        name: 'Colorado Rifle Club',
      },
      times: {
        'Shooter&#39;s Meeting': '9:30 AM'
      },
      contacts: [
        {
          position: 'President',
          name: 'Mark Passamaneck',
          email: 'markpcolo@gmail.com',
        },{
          position: 'Match Director',
          name: 'Steve Cline',
          email: 'thedeadeyemethod@att.net',
        },{
          position: 'Safety Instructor',
          name: 'Mark Passamaneck',
          email: 'markpcolo@gmail.com',
        }
      ],
      fees: {
        'Fee': '$20',
      },
      googleMapsIframeUrl: 'https://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=76099+E.+96th+Ave.+Byers,+CO+80103+&aq=&sll=39.690129,-105.126248&sspn=0.202898,0.295601&ie=UTF8&hq=&hnear=76099+E+96th+Ave,+Byers,+Colorado+80103&t=m&ll=39.848612,-104.099579&spn=0.384828,0.499878&z=10&iwloc=&output=embed',
      googleMapLink: 'https://maps.google.com/maps?f=q&source=embed&hl=en&geocode=&q=76099+E.+96th+Ave.+Byers,+CO+80103+&aq=&sll=39.690129,-105.126248&sspn=0.202898,0.295601&ie=UTF8&hq=&hnear=76099+E+96th+Ave,+Byers,+Colorado+80103&t=m&ll=39.848612,-104.099579&spn=0.384828,0.499878&z=10&iwloc=',
    }, {
      id: 3,
      shortName: "YVPS",
      location: "Steamboat",
      schedules: {
        2019: {
          dayOfWeek: 7,
          weekOfMonth: 2,
          sectionQualifier: 10,
          iffy: [1,2,3,4,11,12],
        },
        2020: {
          dayOfWeek: 7,
          weekOfMonth: 2,
          sectionQualifier: 10,
          iffy: [1,2,3,4,11,12],
        },
        2021: {
          dayOfWeek: 7,
          weekOfMonth: 2,
          iffy: [1,2,3,4,11,12],
          sectionQualifier: 10,
        },
        2022: {
          dayOfWeek: 7,
          weekOfMonth: 2,
          iffy: [1,2,3,4,11,12],
          sectionQualifier: 10,
        },
      },
      name: 'Yampa Valley Practical Shooters',
      code: 'ECO14',
      hostClub: {
        url: 'http://www.routtcountyrifleclub.org',
        name: 'Routt County Rifle Club',
      },
      times: {
        'Shooter&#39;s Meeting': '9:00 AM',
      },
      contacts: [
        {
          position: 'President',
          name: 'Yoshi Yonekawa',
          email: 'yampavalleypracticalshooters@gmail.com',
          phone: '970-879-1270'
        },{
          position: 'Safety Instructor',
          name: 'N/A',
        }
      ],
      fees: {
          'Member fee': '$15',
          'Non-member fee': '$20',
          'Annual dues': 'N/A',
      },
      googleMapsIframeUrl: 'https://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=40.50749,+-106.88699&aq=&sll=40.507502,-106.886287&sspn=0.006265,0.009238&t=m&gl=us&ie=UTF8&ll=40.507535,-106.887016&spn=0.095278,0.124969&z=12&output=embed',
      googleMapLink: 'https://maps.google.com/maps?f=q&source=embed&hl=en&geocode=&q=40.50749,+-106.88699&aq=&sll=40.507502,-106.886287&sspn=0.006265,0.009238&t=m&gl=us&ie=UTF8&ll=40.507535,-106.887016&spn=0.095278,0.124969&z=12',
    }, {
      id: 4,
      shortName: "BRC",
      location: "Boulder",
      schedules: {
        2019: {
          dayOfWeek: 1,
          weekOfMonth: 2,
          sectionQualifier: 8,
        },
        2020: {
          dayOfWeek: 1,
          weekOfMonth: 2,
          omit: [4,5,6,7,8,9,10,11,12],
        },
        2021: {
          dayOfWeek: 1,
          weekOfMonth: 2,
          omit: [1,2,3],
          sectionQualifier: 9,
        },
        2022: {
          dayOfWeek: 1,
          weekOfMonth: 2,
          sectionQualifier: 8,
          override: {
            m8: '28',
          },
        },
      },
      name: 'Boulder Rifle Club',
      code: 'ECO02',
      hostClub: {
        url: 'http://www.boulderrifleclub.com',
        name: 'Boulder Rifle Club',
      },
      times: {
        'Shooter&#39;s Meeting': '9:00 AM',
      },
      contacts: [
        {
          position: 'President',
          name: 'Henning Wallgren',
          email: 'brcuspsamatch@gmail.com',
          phone: '720-999-6512'
        },{
          position: 'Safety Instructor',
          name: 'Charlie Rollins',
          email: 'brcuspsasafety@gmail.com',
          phone: '720-244-0652'
        }
      ],
      fees: {
          'Member fee': '$15',
          'Non-member fee': '$20',
          'Annual dues': '(Boulder Rifle Club Membership)',
      },
      googleMapsIframeUrl: 'https://maps.google.com/maps?ie=UTF8&q=boulder+rifle+club&fb=1&gl=us&hq=boulder+rifle+club&hnear=boulder+rifle+club&cid=0,0,11207013910803477445&t=m&ll=40.05495,-105.254688&spn=0.047959,0.062485&z=13&iwloc=&output=embed',
      googleMapLink: 'https://maps.google.com/maps?ie=UTF8&q=boulder+rifle+club&fb=1&gl=us&hq=boulder+rifle+club&hnear=boulder+rifle+club&cid=0,0,11207013910803477445&t=m&ll=40.05495,-105.254688&spn=0.047959,0.062485&z=13&iwloc=&source=embed',
    }, {
      id: 5,
      shortName: "PPPS",
      location: "Pueblo",
      schedules: {
        2019: {
          dayOfWeek: 1,
          weekOfMonth: 2,
          sectionQualifier: 4,
          superClassifiers: [10],
        },
        2020: {
          dayOfWeek: 1,
          weekOfMonth: 2,
          sectionQualifier: 11,
        },
        2021: {
          dayOfWeek: 1,
          weekOfMonth: 2,
          sectionQualifier: 11,
          superClassifiers: [10],
        },
        2022: {
          dayOfWeek: 1,
          weekOfMonth: 2,
          sectionQualifier: 10,
        },
      },
      name: 'Pike&#39;s Peak Practical Shooters',
      code: 'ECO07',
      hostClub: {
        url: 'http://pwsa.us',
        name: 'Pueblo West Sportsman&#39;s Association',
      },
      times: {
        'Setup start': '6:30 AM',
        'Registration': '8:30 - 09:15 AM',
        'Shooter&#39;s Meeting': '9:30 AM',
      },
      contacts: [
        {
          position: 'President',
          name: 'Brandyn Ness',
          email: 'brandyn.e.ness@gmail.com',
          phone: '719-963-7219'
        },{
          position: 'Safety Instructor',
          name: 'Tom Freeman',
          email: 'puebloshooter@gmail.com',
          phone: '719-660-5442'
        }
      ],
      fees: {
        'Member fee': '$15 ($5 family)',
        'Non-member fee': '$20 ($5 family)',
        'Annual dues': '$25 ($35 family)'
      },
      googleMapsIframeUrl: 'https://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=38.40688,+-104.70803&aq=&sll=39.690129,-105.126248&sspn=0.202898,0.295601&ie=UTF8&t=m&ll=38.412441,-104.664688&spn=0.09819,0.124969&z=12&output=embed',
      googleMapLink: 'https://maps.google.com/maps?f=q&source=embed&hl=en&geocode=&q=38.40688,+-104.70803&aq=&sll=39.690129,-105.126248&sspn=0.202898,0.295601&ie=UTF8&t=m&ll=38.412441,-104.664688&spn=0.09819,0.124969&z=12',
    }, {
      id: 6,
      shortName: "HPPS",
      location: "Ramah",
      schedules: {
        2019: {
          dayOfWeek: 7,
          weekOfMonth: 3,
          sectionQualifier: 8,
        },
        2020: {
          dayOfWeek: 7,
          weekOfMonth: 3,
          sectionQualifier: 8,
          majors: {
            m9: '18-20<sup>2</sup>',
          },
        },
        2021: {
          dayOfWeek: 7,
          weekOfMonth: 3,
          sectionQualifier: 8,
          majors: {
            m7: '16-18<sup>3</sup>',
          },
        },
        2022: {
          dayOfWeek: 7,
          weekOfMonth: 3,
          sectionQualifier: 8,
        },
      },
      name: 'High Plains Practical Shooters',
      code: 'ECO12',
      hostClub: {
        url: 'http://www.blgc.org',
        name: 'Ben Lomond Gun Club',
      },
      contacts: [
        {
          position: 'President',
          name: 'Charlie Perez',
          email: 'rezman@hotmail.com',
          phone: '303-882-6728'
        },{
          position: 'Safety Instructor',
          name: 'Matt Gallant',
          email: 'gallantcodes@gmail.com',
        }
      ],
      times: {
        'Shooter&#39;s Meeting': '9:45 AM'
      },
      fees: {
        'Member fee': '$15',
        'Non-member fee': '$20',
        'Annual dues': 'N/A',
      },
      googleMapsIframeUrl: 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3091.7099723298547!2d-104.18181158437356!3d39.20403843641516!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x876d2eb9b0e83309%3A0xff3370273021a6e9!2s22615+Co+Rd+105%2C+Simla%2C+CO+80835!5e0!3m2!1sen!2sus!4v1486688108865',
      googleMapLink: 'https://www.google.com/maps?ll=39.204034,-104.179623&z=16&t=m&hl=en-US&gl=US&mapclient=embed&q=22615+Co+Rd+105+Simla,+CO+80835',
    }, {
      id: 7,
      shortName: "WCPS",
      location: "Weld County",
      schedules: {
        2019: {
          dayOfWeek: 1,
          weekOfMonth: 3,
          sectionQualifier: 11,
          majors: {
            m6: 'May 31 - Jun 3<sup>1</sup>',
          },
        },
        2020: {
          dayOfWeek: 1,
          weekOfMonth: 3,
          sectionQualifier: 8,
          omit: [9],
          override: {
            m10: '<span class="major">9-11<sup>1</sup></span>, 18, 31',
          }
        },
        2021: {
          dayOfWeek: 1,
          weekOfMonth: 3,
          sectionQualifier: 5,
          omit: [7],
          override: {
            m6: '<span class="major">4-6<sup>1</sup></span>, <span class="super-classifier">20</span>',
            m8: '29',
          },
        },
        2022: {
          dayOfWeek: 1,
          weekOfMonth: 3,
          override: {
            m7: '17, <span class="section-qualifier">31</span>',
          },
        },
      },
      name: 'Weld County Practical Shooters',
      code: 'ECO09',
      hostClub: {
        url: 'http://wcfw.org',
        name: 'Weld County Fish & Wildlife',
      },
      times: {
        'Setup start': '7:30 AM',
        'New shooter safety briefing': '8:00 AM',
        'Registration': '<a target="_BLANK" href="https://practiscore.com/clubs/weld_county_practical_shooters">Click Here</a>',
        'The front gate locks': '9:45 AM',
        'The shooter&#39;s meeting': '10 AM',
      },
      contacts: [
        {
          position: 'President',
          name: 'Patrick Jones',
          email: 'uspsa@wcfw.org',
        }
      ],
      fees: {
        'Member fee': '$15',
        'Non-member fee': '$20',
        'Annual dues': '$30',
      },
      googleMapsIframeUrl: 'https://maps.google.com/maps?q=38967+CR+51+Eaton,+CO+80615&ie=UTF8&hq=&hnear=38967+County+Road+51,+Eaton,+Colorado+80615&gl=us&t=m&ll=40.488737,-104.773865&spn=0.381215,0.499878&z=10&iwloc=&output=embed',
      googleMapLink: 'https://maps.google.com/maps?q=38967+CR+51+Eaton,+CO+80615&ie=UTF8&hq=&hnear=38967+County+Road+51,+Eaton,+Colorado+80615&gl=us&t=m&ll=40.488737,-104.773865&spn=0.381215,0.499878&z=10&iwloc=&source=embed',
    }, {
      id: 8,
      shortName: "PSAC",
      location: "Aurora (Saturday)",
      schedules: {
        2019: {
          dayOfWeek: 7,
          weekOfMonth: 4,
          omit: [4,5,6,7,8,9,10,11,12],
        },
      },
    }, {
      id: 9,
      shortName: "CCPS",
      location: "Dumont",
      schedules: {
        2019: {
          dayOfWeek: 1,
          weekOfMonth: 4,
          sectionQualifier: 7,
          iffy: [1,2,11,12],
        },
        2020: {
          dayOfWeek: 1,
          weekOfMonth: 4,
          sectionQualifier: 7,
          iffy: [1,2,11,12],
        },
        2021: {
          dayOfWeek: 1,
          weekOfMonth: 4,
          sectionQualifier: 7,
          iffy: [1,2,11,12],
        },
        2022: {
          dayOfWeek: 1,
          weekOfMonth: 4,
          iffy: [1,2,11,12],
          sectionQualifier: 6,
        },
      },
      name: 'Clear Creek Practical Shooters',
      code: 'ECO06',
      hostClub: {
        url: 'http://www.cccsclub.com',
        name: 'Clear Creek County Sportsman&#39;s Club',
      },
      times: {
        'Shooter&#39;s Meeting': '9:00 AM',
      },
      contacts: [
        {
          position: 'President',
          name: 'Roger Sorge',
          email: 'wfwsorge@comcast.net',
          phone: ''
        },{
          position: 'Safety Instructor',
          name: 'N/A',
        }
      ],
      fees: {
        'Member fee': '$20',
        'Non-member fee': '$20',
        'Annual dues': 'N/A',
      },
      googleMapsIframeUrl: 'https://maps.google.com/maps?ie=UTF8&q=clear+creek+county+sportsman\'s+club&fb=1&gl=us&hq=sportsman\'s+club&hnear=0x876ba533edc270af:0x5ef585dc43a743f,Clear+Creek,+CO&cid=0,0,8919952254577080246&t=m&ll=39.762829,-105.586939&spn=0.024082,0.031242&z=14&iwloc=&output=embed',
      googleMapLink: 'https://maps.google.com/maps?ie=UTF8&q=clear+creek+county+sportsman\'s+club&fb=1&gl=us&hq=sportsman\'s+club&hnear=0x876ba533edc270af:0x5ef585dc43a743f,Clear+Creek,+CO&cid=0,0,8919952254577080246&t=m&ll=39.762829,-105.586939&spn=0.024082,0.031242&z=14&iwloc=&source=embed',
    }, {
      id: 10,
      shortName: "SCPS",
      location: "Colorado Springs",
      schedules: {
        2019: {
          dayOfWeek: 7,
          weekOfMonth: 4,
          sectionQualifier: 6,
          omit: [1,2,3],
          superClassifiers: [8],
        },
        2020: {
          dayOfWeek: 7,
          weekOfMonth: 4,
          sectionQualifier: 7,
          omit: [1],
          superClassifiers: [8],
        },
        2021: {
          dayOfWeek: 7,
          weekOfMonth: 4,
          sectionQualifier: 9,
          omit: [6, 12],
        },
        2022: {
          dayOfWeek: 7,
          weekOfMonth: 4,
          sectionQualifier: 7,
          omit: [4, 12],
        },
      },
      name: 'SoCo Practical Shooters',
      code: 'ECO19',
      hostClub: {
        url: 'http://pikespeakgunclub.org/',
        name: 'Pikes Peak Gun Club',
      },
      times: {
        'Setup Start': '7:00',
        'New Shooter Safety Briefing': '8:00',
        'Registration': '8:30-9:30',
        'Shooters Meeting': '9:45',
      },
      contacts: [
        {
          position: 'Match Director',
          name: 'Germaine Adams',
          email: 'socopracticalshooters@gmail.com',
          phone: '719-439-6993'
        },
        {
          position: 'Safety Instructor',
          name: 'Joel Whittington',
          email: 'jwhittin@yahoo.com',
        },
      ],
      fees: {
        'Member Fee': '$25',
        'Non-Member Fee': '$30',
        'Annual Dues': '$40'
      },
      googleMapsIframeUrl: 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3108.3353142808855!2d-104.59708568465177!3d38.82478087958187!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8713393be14db38d%3A0xf0179b39a2e04b1e!2s450+S+Franceville+Coal+Mine+Rd%2C+Colorado+Springs%2C+CO+80929!5e0!3m2!1sen!2sus!4v1547002830549',
      googleMapLink: 'https://goo.gl/maps/949RTKCAx4H2',
    }, {
      id: 11,
      shortName: "WPPS",
      location: "Colorado Springs",
      schedules: {
        2019: {
          dayOfWeek: 2,
          weekOfMonth: 1,
        },
        2020: {
          dayOfWeek: 2,
          weekOfMonth: 1,
          omit: [9],
        },
        2021: {
          dayOfWeek: 2,
          weekOfMonth: 1,
          omit: [9],
        },
        2022: {
          dayOfWeek: 2,
          weekOfMonth: 1,
        },
      },
      name: 'Whistling Pines Practical Shooters',
      code: 'ECO13',
      hostClub: {
        url: 'http://www.whistlingpinesgunclub.com',
        name: 'Whistling Pines Gun Club',
      },
      times: {
        'Shooter&#39;s Meeting': '7:00 PM',
      },
      contacts: [
        {
          position: 'President',
          name: 'Tom Freeman',
          email: 'WPGCResults@gmail.com',
          phone: '719-660-5442'
        },{
          position: 'Safety Instructor',
          name: 'Tom Freeman',
          email: 'WPGCResults@gmail.com',
        }
      ],
      fees: {
        'Member fee': '$10',
        'Non-member fee': '$15',
        'Annual dues': '$35 (one time)',
      },
      googleMapsIframeUrl: 'https://maps.google.com/maps?hl=en&ie=UTF8&q=whistling+pines+gun+club&fb=1&gl=us&hq=whistling+pines+gun+club&cid=0,0,1414679191610636993&t=m&ll=38.848264,-104.688034&spn=0.097595,0.124969&z=12&iwloc=&output=embed',
      googleMapLink: 'https://maps.google.com/maps?hl=en&ie=UTF8&q=whistling+pines+gun+club&fb=1&gl=us&hq=whistling+pines+gun+club&cid=0,0,1414679191610636993&t=m&ll=38.848264,-104.688034&spn=0.097595,0.124969&z=12&iwloc=&source=embed',
    }, {
      id: 12,
      shortName: "BPS",
      location: "Lakewood",
      schedules: {
        2019: {
          dayOfWeek: 5,
          weekOfMonth: [1, 3],
        },
        2020: {
          dayOfWeek: 5,
          weekOfMonth: [1, 3],
          override: {
            m3: '<span class="super-classifier">5,</span> 19',
          }
        },
        2021: {
          dayOfWeek: 5,
          weekOfMonth: [1, 3],
          override: {
            m5: '6, <span class="super-classifier">20</span>',
          }
        },
        2022: {
          dayOfWeek: 5,
          weekOfMonth: [1, 3],
        },
      },
      name: 'Bristlecone Practical Shooters',
      code: 'ECO17',
      hostClub: {
        url: 'http://www.bristleconeshooting.com/',
        name: 'Bristlecone Shooting, Training & Retail Center',
      },
      times: {
        'Registration': '5:30 PM',
        'Shooter&#39;s Meeting': '6:00 PM',
      },
      contacts: [
        {
          position: 'President',
          name: 'Bryan Clark',
          email: 'bjclarkgm@gmail.com',
          phone: '720-557-6925'
        },
      ],
      fees: {
        'Member fee': '$22.50',
        'Non-member fee': '$25',
        'Super Classifier fee': '$40',
      },
      googleMapsIframeUrl: 'https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d6138.410216386874!2d-105.135754!3d39.712573!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x876b83fa3bf856e1%3A0xc25a9d7fc7ee9f6f!2sBristlecone+Shooting%2C+Training%2C+%26+Retail+Center!5e0!3m2!1sen!2sus!4v1486524626955',
      googleMapLink: 'https://www.google.com/maps/place/Bristlecone+Shooting,+Training,+%26+Retail+Center/@39.712573,-105.135754,15z/data=!4m5!3m4!1s0x0:0xc25a9d7fc7ee9f6f!8m2!3d39.712573!4d-105.135754',
    }, {
      id: 13,
      shortName: "AR&PC",
      location: "Arvada",
      schedules: {
        2020: {
          dayOfWeek: 2,
          weekOfMonth: 3,
          omit: [1,2,3,4,5,6,7,8,9,10],
        },
        2021: {
          dayOfWeek: 2,
          weekOfMonth: 3,
          superClassifiers: [4],
        },
        2022: {
          dayOfWeek: 2,
          weekOfMonth: 3,
        },
      },
      name: 'Arvada Rifle and Pistol Practical Shooters',
      code: 'ECO20',
      hostClub: {
        url: 'https://www.arpcinc.com/USPSA',
        name: 'Arvada Rifle and Pistol Club',
      },
      times: {
        'Registration': '5:00-5:45 pm',
        'New Shooter Safety Briefing': '5:00 pm',
        'Shooters Meeting': '5:45 pm',
      },
      contacts: [
        {
          position: 'President',
          name: 'Jon Manna',
          email: 'jcmanna@comcast.net',
          phone: '720-641-1919'
        },{
          position: 'Safety Instructor',
          name: 'Matthew Krause',
          email: 'mkrause.design@gmail.com',
          phone: '818-590-5763'
        }
      ],
      fees: {
        'Member Fee': '$20',
        'Non-member Fee': '$25',
      },
      googleMapsIframeUrl: 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3065.210536992554!2d-105.17386968420213!3d39.80226897944134!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x876b8f78646abee3%3A0x3b4317cc98605024!2s5930%20McIntyre%20St%2C%20Golden%2C%20CO%2080403!5e0!3m2!1sen!2sus!4v1605020512402!5m2!1sen!2sus',
      googleMapLink: 'https://goo.gl/maps/s3AUBpUUAJLCN6t18',
    }, {
      id: 14,
      shortName: "CGC",
      location: "Centennial",
      schedules: {
        2022: {
          dayOfWeek: 1,
          weekOfMonth: 4,
          omit: [1, 2],
        },
      },
      name: 'Centennial Gun Club',
      code: 'ECO16',
      hostClub: {
        url: 'https://centennialgunclub.com/events2/',
        name: 'Centennial Gun Club',
      },
      times: {
        'Setup Start': '6:00 pm',
        'Registration': '5:30 pm - 6:30 pm',
        'Shooter\'s Meeting': '7:00 pm',
      },
      contacts: [
        {
          position: 'President',
          name: 'Joey Mizufuka',
          phone: '303-397-3281'
        },{
          position: 'Range Master',
          name: 'Richard Clare',
        },{
          position: 'Stats/Scoring',
          name: 'Sean Myhre',
        },{
          position: 'Stage Design',
          name: 'Spencer Stein, Rodney Lim, Alex Iglesias, and Jeff Schroeder',
        },
      ],
      fees: {
        'Fee': '$25',
      },
      googleMapsIframeUrl: 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3074.3558040454186!2d-104.85468868427866!3d39.59666667946895!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x876c807d31cd9269%3A0xc184657f39a34cc2!2sCentennial%20Gun%20Club!5e0!3m2!1sen!2sus!4v1648307210971!5m2!1sen!2sus',
      googleMapLink: 'https://g.page/CentennialGunClub?share',
    },
  ],
  scsa: [
    {
      id: 7,
      shortName: "WCPS",
      location: "Weld County",
      schedules: {
        2022: {
          dayOfWeek: 7,
          weekOfMonth: 1,
          majors: {
            m8: '5-7<sup>1</sup>',
            m9: '3<sup>2</sup>',
          },
        },
      },
      name: 'Weld County Practical Shooters',
      code: 'ECO09',
      hostClub: {
        url: 'http://wcfw.org',
        name: 'Weld County Fish & Wildlife',
      },
      times: {
        'Shooting Starts': '10 AM',
      },
      contacts: [
        {
          position: 'President',
          name: 'Pat Miller',
          email: 'steelchallenge@wcfw.org',
          phone: '970-888-1902',
        }
      ],
      fees: {
        'Member fee': '$15',
        'Non-member fee': '$20',
        'Annual dues': '$30',
      },
      googleMapsIframeUrl: 'https://maps.google.com/maps?q=38967+CR+51+Eaton,+CO+80615&ie=UTF8&hq=&hnear=38967+County+Road+51,+Eaton,+Colorado+80615&gl=us&t=m&ll=40.488737,-104.773865&spn=0.381215,0.499878&z=10&iwloc=&output=embed',
      googleMapLink: 'https://maps.google.com/maps?q=38967+CR+51+Eaton,+CO+80615&ie=UTF8&hq=&hnear=38967+County+Road+51,+Eaton,+Colorado+80615&gl=us&t=m&ll=40.488737,-104.773865&spn=0.381215,0.499878&z=10&iwloc=&source=embed',
    }, {
      id: 8,
      shortName: "CRCPS",
      location: "Byers",
      schedules: {
        2022: {
          dayOfWeek: 7,
          weekOfMonth: 3,
          override: {
            m4: '30',
          }
        },
      },
      name: 'Colorado Rifle Club Practical Shooters',
      code: 'ECO15',
      hostClub: {
        url: 'http://www.crci.org',
        name: 'Colorado Rifle Club',
      },
      times: {},
      contacts: [],
      fees: {
        'Fee': '$20',
      },
      googleMapsIframeUrl: 'https://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=76099+E.+96th+Ave.+Byers,+CO+80103+&aq=&sll=39.690129,-105.126248&sspn=0.202898,0.295601&ie=UTF8&hq=&hnear=76099+E+96th+Ave,+Byers,+Colorado+80103&t=m&ll=39.848612,-104.099579&spn=0.384828,0.499878&z=10&iwloc=&output=embed',
      googleMapLink: 'https://maps.google.com/maps?f=q&source=embed&hl=en&geocode=&q=76099+E.+96th+Ave.+Byers,+CO+80103+&aq=&sll=39.690129,-105.126248&sspn=0.202898,0.295601&ie=UTF8&hq=&hnear=76099+E+96th+Ave,+Byers,+Colorado+80103&t=m&ll=39.848612,-104.099579&spn=0.384828,0.499878&z=10&iwloc=',
    },
  ],
  majors: {
    2022: [
      {
        start: '3/30',
        end: '4/3',
        title: 'Multigun Nationals',
        location: 'Clinton, SC',
        price: 295,
      }, {
        start: '4/20',
        end: '4/24',
        title: 'Colorado State - Big Horn Classic',
        location: 'Palisade, CO',
        price: 150,
      }, {
        start: '5/20',
        end: '5/22',
        title: 'Classic Nationals (SS, L10, Revo)',
        location: 'Talladega, AL',
        price: 250,
      }, {
        start: '6/2',
        end: '6/5',
        title: 'Area 1 Championship',
        location: 'Nampa, ID',
        price: 200,
      }, {
        start: '6/9',
        end: '6/12',
        title: '2-Gun PCC/Pistol Nationals',
        location: 'Palisade, CO',
        price: 295,
      }, {
        start: '6/10',
        end: '6/12',
        title: 'Rocky Mountain 300',
        location: 'Weld, CO',
        price: 99,
      }, {
        start: '8/11',
        end: '8/14',
        title: 'Area 3 Championship',
        location: 'Alda, NE',
      }, {
        start: '9/1',
        end: '9/4',
        title: 'Utah State',
        location: 'Salt Lake City, UT',
        price: 160,
      }, {
        start: '9/7',
        end: '9/11',
        title: 'Carry Optics Nationals',
        location: 'Talladega, AL',
        price: 285,
      }, {
        start: '9/22',
        end: '9/25',
        title: 'Area 4 Championship',
        location: 'Tulsa, OK',
        price: 185,
      }, {
        start: '9/23',
        end: '9/25',
        title: 'US IPSC Nationals',
        location: 'Frostproof, FL',
        price: 210,
      }, {
        start: '10/11',
        end: '10/16',
        title: 'Open/Limited/PCC/Production Nationals',
        location: 'Palisade, CO',
        price: 295,
      }, {
        start: '11/10',
        end: '11/13',
        title: 'Area 2 Championship',
        location: 'Mesa, AZ',
        price: 330,
      },
    ]
  },
})
